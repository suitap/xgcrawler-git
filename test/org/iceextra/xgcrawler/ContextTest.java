package org.iceextra.xgcrawler;

import static org.junit.Assert.assertNotNull;

import java.util.Map;
import java.util.Map.Entry;

import org.iceextra.xgcrawler.Context;
import org.iceextra.xgcrawler.helper.SkillNoteHelper;
import org.iceextra.xgcrawler.model.Result;
import org.junit.Test;

public class ContextTest extends Context {

	public ContextTest(String configName) {
		super(configName);
	}

	@Test
	public void test() {
		start();
		try {
			Map<String, Result> results = getGFAllMusics();
			assertNotNull(results);
			SkillNoteHelper.setSkillEditModeToGf(skillnote);
			for (Entry<String, Result> _result : results.entrySet()) {
				Result result = _result.getValue();
				SkillNoteHelper.updateSkillNote(skillnote, result, false, false);
			}
		} finally {
			end();
		}
	}

}
