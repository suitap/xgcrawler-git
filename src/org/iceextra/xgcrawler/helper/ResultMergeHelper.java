package org.iceextra.xgcrawler.helper;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.iceextra.xgcrawler.model.Music;
import org.iceextra.xgcrawler.model.Result;

public class ResultMergeHelper {

	public static void mergeResult(Map<String, Result> results,
			Map<String, Music> musics) {
		for (Entry<String, Result> _result : results.entrySet()) {
			Result result = _result.getValue();
			for (Entry<String, Music> entry : musics.entrySet()) {
				Music music = entry.getValue();
				if (StringUtils.equals(music.name, result.music.name)) {
					result.music.skillnoteId = music.skillnoteId;
					result.music.skillnoteName = music.skillnoteName;
					result.music.isNew = music.isNew;
					result.music.difficulties.addAll(music.difficulties);
				}
			}
		}
	}

}
