package org.iceextra.xgcrawler.helper;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.iceextra.xgcrawler.model.types.Level;
import org.iceextra.xgcrawler.model.types.Type;
import org.junit.Test;

public class SkillNoteHelperTest extends SkillNoteHelper {

	@Test
	public void testLevelValues() {
		assertEquals("BSC|ADV|EXT|MAS", StringUtils.join(Level.values(), "|"));
	}

	@Test
	public void testGetOriginalComment() {
		assertEquals("", getOriginalComment(null));
		assertEquals("", getOriginalComment(""));

		assertEquals("a", getOriginalComment("a"));

		assertEquals("", getOriginalComment("ADV:0.00%(0.00)"));
		assertEquals("a", getOriginalComment("MAS(B):0.00%(0.00)a"));
		assertEquals("a", getOriginalComment("aEXT:0.00%(0.00)"));
		assertEquals("ADV", getOriginalComment("ADVEXT(G):0.00%(0.00)"));
		assertEquals("ADV", getOriginalComment("BSC:0.00%(0.00)ADV"));
		assertEquals("ADV ADV", getOriginalComment("ADVADV(B):0.00%(0.00)ADV"));
		assertEquals("aaa bbb", getOriginalComment("aaa ADV:0.00%(0.00) bbb"));

		assertEquals("",
				getOriginalComment("ADV:0.00%(0.00) BSC:99.00%(99.00)"));
		assertEquals("a",
				getOriginalComment("ADV(B):0.00%(0.00) BSC(G):99.00%(99.00)a"));
		assertEquals("a",
				getOriginalComment("aADV:0.00%(0.00) BSC:99.00%(99.00)"));
		assertEquals(
				"ADV",
				getOriginalComment("ADVEXT(G):0.00%(0.00) BSC(B):99.00%(99.00)"));
		assertEquals("ADV",
				getOriginalComment("ADV:0.00%(0.00) BSC:99.00%(99.00)ADV"));
		assertEquals(
				"ADV ADV",
				getOriginalComment("ADVEXT(B):0.00%(0.00) BSC(G):99.00%(99.00)ADV"));
		assertEquals("aaa bbb",
				getOriginalComment("aaa ADV:0.00%(0.00) BSC:99.00%(99.00) bbb"));

		assertEquals("", getOriginalComment("ADV:0.00%(0.00)BSC:99.00%(99.00)"));
		assertEquals("a",
				getOriginalComment("ADV(B):0.00%(0.00)BSC(G):99.00%(99.00)a"));
		assertEquals("a",
				getOriginalComment("aADV:0.00%(0.00)BSC:99.00%(99.00)"));
		assertEquals("ADV",
				getOriginalComment("ADVEXT(G):0.00%(0.00)BSC(B):99.00%(99.00)"));
		assertEquals("ADV",
				getOriginalComment("ADV:0.00%(0.00)BSC:99.00%(99.00)ADV"));
		assertEquals(
				"ADV ADV",
				getOriginalComment("ADVEXT(B):0.00%(0.00)BSC(G):99.00%(99.00)ADV"));
		assertEquals("aaa bbb",
				getOriginalComment("aaa ADV:0.00%(0.00)BSC:99.00%(99.00) bbb"));

		assertEquals("", getOriginalComment("*ADV:0.00%(0.00)"));
		assertEquals("a", getOriginalComment("*MAS(B):0.00%(0.00)a"));
		assertEquals("a", getOriginalComment("a*EXT:0.00%(0.00)"));
		assertEquals("ADV", getOriginalComment("ADV*EXT(G):0.00%(0.00)"));
		assertEquals("ADV", getOriginalComment("*BSC:0.00%(0.00)ADV"));
		assertEquals("ADV ADV", getOriginalComment("ADV*ADV(B):0.00%(0.00)ADV"));
		assertEquals("aaa bbb", getOriginalComment("aaa *ADV:0.00%(0.00) bbb"));

		assertEquals("",
				getOriginalComment("*ADV:0.00%(0.00) BSC:99.00%(99.00)"));
		assertEquals("a",
				getOriginalComment("*ADV(G):0.00%(0.00) BSC(B):99.00%(99.00)a"));
		assertEquals("a",
				getOriginalComment("a*ADV:0.00%(0.00) BSC:99.00%(99.00)"));
		assertEquals(
				"ADV",
				getOriginalComment("ADV*ADV(B):0.00%(0.00) BSC(G):99.00%(99.00)"));
		assertEquals("ADV",
				getOriginalComment("*ADV:0.00%(0.00) BSC:99.00%(99.00)ADV"));
		assertEquals(
				"ADV ADV",
				getOriginalComment("ADV*EXT(G):0.00%(0.00) *BSC(B):99.00%(99.00)ADV"));
		assertEquals(
				"aaa bbb",
				getOriginalComment("aaa *ADV:0.00%(0.00) BSC:99.00%(99.00) bbb"));

		assertEquals("",
				getOriginalComment("*ADV:0.00%(0.00)BSC:99.00%(99.00)"));
		assertEquals("a",
				getOriginalComment("*ADV(B):0.00%(0.00)BSC(G):99.00%(99.00)a"));
		assertEquals("a",
				getOriginalComment("a*ADV:0.00%(0.00)BSC:99.00%(99.00)"));
		assertEquals(
				"ADV",
				getOriginalComment("ADV*ADV(G):0.00%(0.00)BSC(B):99.00%(99.00)"));
		assertEquals("ADV",
				getOriginalComment("*ADV:0.00%(0.00)BSC:99.00%(99.00)ADV"));
		assertEquals(
				"ADV ADV",
				getOriginalComment("ADV*EXT(B):0.00%(0.00)*BSC(G):99.00%(99.00)ADV"));
		assertEquals("aaa bbb",
				getOriginalComment("aaa *ADV:0.00%(0.00)BSC:99.00%(99.00) bbb"));
	}

	@Test
	public void testIsSkillUpdateExpected() {
		assertEquals(false, isSkillUpdateExpected(null, null, null, null, 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.BSC, new BigDecimal(80), Level.BSC,
						new BigDecimal(75), 0));

		assertEquals(
				true,
				isSkillUpdateExpected(Level.BSC, new BigDecimal(80), Level.BSC,
						new BigDecimal(75), 10)); // G / B 比較
		assertEquals(
				false,
				isSkillUpdateExpected(Level.BSC, new BigDecimal(80), Level.ADV,
						new BigDecimal(85), 10));

		assertEquals(
				true,
				isSkillUpdateExpected(Level.BSC, new BigDecimal(80), Level.ADV,
						new BigDecimal(75), 10));
		assertEquals(
				true,
				isSkillUpdateExpected(Level.BSC, new BigDecimal(80), Level.EXT,
						new BigDecimal(70), 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.BSC, new BigDecimal(80), Level.MAS,
						new BigDecimal(65), 10));

		assertEquals(
				true,
				isSkillUpdateExpected(Level.ADV, new BigDecimal(80), Level.EXT,
						new BigDecimal(75), 10));
		assertEquals(
				true,
				isSkillUpdateExpected(Level.ADV, new BigDecimal(80), Level.MAS,
						new BigDecimal(70), 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.ADV, new BigDecimal(80), Level.BSC,
						new BigDecimal(65), 10));

		assertEquals(
				true,
				isSkillUpdateExpected(Level.EXT, new BigDecimal(80), Level.MAS,
						new BigDecimal(75), 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.EXT, new BigDecimal(80), Level.BSC,
						new BigDecimal(70), 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.EXT, new BigDecimal(80), Level.ADV,
						new BigDecimal(65), 10));

		assertEquals(
				false,
				isSkillUpdateExpected(Level.MAS, new BigDecimal(80), Level.BSC,
						new BigDecimal(75), 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.MAS, new BigDecimal(80), Level.ADV,
						new BigDecimal(70), 10));
		assertEquals(
				false,
				isSkillUpdateExpected(Level.MAS, new BigDecimal(80), Level.EXT,
						new BigDecimal(65), 10));
	}

	@Test
	public void testGetSeqTypeString() {
		assertEquals("", getSeqTypeString(null, null));
		assertEquals("", getSeqTypeString(null, Type.D));
		assertEquals("", getSeqTypeString(Level.BSC, null));

		assertEquals("BSC", getSeqTypeString(Level.BSC, Type.D));
		assertEquals("BSC(G)", getSeqTypeString(Level.BSC, Type.G));
		assertEquals("BSC(B)", getSeqTypeString(Level.BSC, Type.B));

		assertEquals("ADV", getSeqTypeString(Level.ADV, Type.D));
		assertEquals("ADV(G)", getSeqTypeString(Level.ADV, Type.G));
		assertEquals("ADV(B)", getSeqTypeString(Level.ADV, Type.B));
	}
}
