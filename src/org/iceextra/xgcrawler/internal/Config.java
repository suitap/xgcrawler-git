package org.iceextra.xgcrawler.internal;

import static org.apache.commons.lang3.BooleanUtils.toBoolean;

import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.iceextra.xgcrawler.exception.CrawlerException;

public class Config {

	private final Properties prop = new Properties();

	private static final Log log = LogFactory.getLog(Config.class);

	public Config(String configName) {
		InputStream is;
		if (configName == null || "".equals(configName)) {
			is = Config.class.getResourceAsStream("/config.xml");
			log.info("設定ファイルを読み込みます.");
		} else {
			is = Config.class.getResourceAsStream("/config-" + configName
					+ ".xml");
			log.info("設定ファイル: " + configName + " を読み込みます.");
		}
		if (is == null) {
			throw new CrawlerException("設定ファイルが存在しません。");
		}
		try {
			prop.loadFromXML(is);
		} catch (InvalidPropertiesFormatException e) {
			throw new CrawlerException(e);
		} catch (IOException e) {
			throw new CrawlerException(e);
		}
	}

	public String getProxyHost() {
		return prop.getProperty("ProxyHost");
	}

	public int getProxyPort() {
		return NumberUtils.toInt(prop.getProperty("ProxyPort"));
	}

	public String getEAGateUsername() {
		return prop.getProperty("EAGateUsername");
	}

	public String getEAGatePassword() {
		return prop.getProperty("EAGatePassword");
	}

	public String getSkillNoteUsername() {
		return prop.getProperty("SkillNoteUsername");
	}

	public String getSkillNotePassword() {
		return prop.getProperty("SkillNotePassword");
	}

	public boolean canUpdateGf() {
		String updateGuitarSkill = prop.getProperty("UpdateGuitarSkill");
		return StringUtils.isEmpty(updateGuitarSkill)
				|| toBoolean(updateGuitarSkill);
	}

	public boolean canUpdateDm() {
		String updateDrumSkill = prop.getProperty("UpdateDrumSkill");
		return StringUtils.isEmpty(updateDrumSkill)
				|| toBoolean(updateDrumSkill);
	}

	public boolean canUpdateOnlySkillTarget() {
		String updateOnlySkillTarget = prop
				.getProperty("UpdateOnlySkillTarget");
		return StringUtils.isNotEmpty(updateOnlySkillTarget)
				&& toBoolean(updateOnlySkillTarget);
	}

	public boolean canSynchronizeCompletely() {
		String synchronizeCompletely = prop
				.getProperty("SynchronizeCompletely");
		return StringUtils.isEmpty(synchronizeCompletely)
				|| toBoolean(synchronizeCompletely);
	}

	public boolean canUpdateFullcombo() {
		String updateFullcombo = prop.getProperty("UpdateFullcombo");
		return StringUtils.isNotEmpty(updateFullcombo)
				&& toBoolean(updateFullcombo);
	}

	public boolean canDeleteAll() {
		String deleteAll = prop.getProperty("DeleteAll");
		return StringUtils.isNotEmpty(deleteAll) && toBoolean(deleteAll);
	}

	public boolean canAddSkillInfoToComment() {
		String addSkillInfoToComment = prop
				.getProperty("AddSkillInfoToComment");
		return StringUtils.isNotEmpty(addSkillInfoToComment)
				&& toBoolean(addSkillInfoToComment);
	}
}
