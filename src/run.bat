@echo off
cd /d %~dp0

echo ************************************************************
echo *       XGCrawler動作中はブラウザやGITADORAアプリで        *
echo *       プレーデータの参照をしないようにしてください。     *
echo ************************************************************
@echo on
XGCrawler.jar

@echo "実行ログ(crawlerlog.txt)を表示します."
@type crawlerlog.txt

pause
