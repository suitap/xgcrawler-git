package org.iceextra.xgcrawler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.iceextra.xgcrawler.exception.CrawlerException;
import org.iceextra.xgcrawler.helper.EAGateHelper;
import org.iceextra.xgcrawler.helper.ResultMergeHelper;
import org.iceextra.xgcrawler.helper.SkillNoteHelper;
import org.iceextra.xgcrawler.internal.Config;
import org.iceextra.xgcrawler.internal.HttpClientWrapper;
import org.iceextra.xgcrawler.model.Music;
import org.iceextra.xgcrawler.model.Result;

public class Context {

	protected Config config;

	protected HttpClientWrapper eagate;

	protected HttpClientWrapper skillnote;

	protected boolean started = false;

	private static final Log log = LogFactory.getLog(Context.class);

	public Context(String configName) {
		config = new Config(configName);
		if (config.canDeleteAll()) {
			log.info("設定: [削除のみ]");
		} else {
			if (config.canUpdateGf() && !config.canUpdateDm()) {
				log.info("設定: [Guitarのみ更新]");
			}
			if (!config.canUpdateGf() && config.canUpdateDm()) {
				log.info("設定: [Drumのみ更新]");
			}
			if (config.canUpdateGf() && config.canUpdateDm()) {
				log.info("設定: [Guitar&Drum更新]");
			}
			if (config.canUpdateOnlySkillTarget()) {
				log.info("設定: [対象曲のみ更新]");
			} else {
				log.info("設定: [全曲更新]");
			}
			if (config.canSynchronizeCompletely()) {
				log.info("設定: [スキル値が(GATE < スキル帳)の場合でも更新]");
			} else {
				log.info("設定: [スキル値が(GATE > スキル帳)の場合のみ更新]");
			}
			if (config.canUpdateFullcombo()) {
				log.info("設定: [FCマーク更新あり]");
			} else {
				log.info("設定: [FCマーク更新なし]");
			}
			if (config.canAddSkillInfoToComment()) {
				log.info("設定: [全リザルト出力あり]");
			} else {
				log.info("設定: [全リザルト出力なし]");
			}
		}
	}

	public void start() {
		eagate = new HttpClientWrapper(config, "Windows-31J");
		skillnote = new HttpClientWrapper(config, "UTF-8");
		EAGateHelper.loginEAGate(eagate, config.getEAGateUsername(),
				config.getEAGatePassword());
		SkillNoteHelper.loginSkillNote(skillnote,
				config.getSkillNoteUsername(), config.getSkillNotePassword());
		started = true;
	}

	public void end() {
		started = false;
		EAGateHelper.logoutEAGate(eagate);
		SkillNoteHelper.logoutSkillNote(skillnote);
		eagate.shutdown();
		skillnote.shutdown();
	}

	public Map<String, Result> getGFAllMusics() {
		if (!started) {
			return null;
		}
		SkillNoteHelper.setSkillEditModeToGf(skillnote);

		Map<String, Music> musics = SkillNoteHelper.loadMusics(skillnote);
		Map<String, Result> results = EAGateHelper.getGfAllResult(eagate,
				musics, config.canUpdateFullcombo());
		ResultMergeHelper.mergeResult(results, musics);
		SkillNoteHelper.mergeGfSkillNoteResult(skillnote, config, results);

		return results;
	}

	public Map<String, Result> getDMAllMusics() {
		if (!started) {
			return null;
		}
		SkillNoteHelper.setSkillEditModeToDm(skillnote);

		Map<String, Music> musics = SkillNoteHelper.loadMusics(skillnote);
		Map<String, Result> results = EAGateHelper.getDmAllResult(eagate,
				musics, config.canUpdateFullcombo());
		ResultMergeHelper.mergeResult(results, musics);
		SkillNoteHelper.mergeDmSkillNoteResult(skillnote, config, results);

		return results;
	}

	public static void main(String[] args) {
		log.info("XGCrawler "
				+ Context.class.getPackage().getImplementationVersion()
				+ " を起動します.");
		Context context = null;
		try {
			if (args == null || args.length <= 0) {
				context = new Context(null);
			} else {
				context = new Context(args[0]);
			}
			if (!context.config.canUpdateGf() && !context.config.canUpdateDm()) {
				log.warn("Guitar/Drumのいずれも更新対象となっていないため終了します.");
				return;
			}

			context.start();

			if (context.config.canDeleteAll()) {
				SkillNoteHelper.deleteAllResult(context.skillnote);
				return;
			}
			if (context.config.canUpdateGf()) {
				Map<String, Result> gfResults = context.getGFAllMusics();
				SkillNoteHelper.setSkillEditModeToGf(context.skillnote);
				updateSkillNote(context, gfResults,
						context.config.canSynchronizeCompletely());
			}
			if (context.config.canUpdateDm()) {
				Map<String, Result> dmResults = context.getDMAllMusics();
				SkillNoteHelper.setSkillEditModeToDm(context.skillnote);
				updateSkillNote(context, dmResults,
						context.config.canSynchronizeCompletely());
			}
		} catch (CrawlerException e) {
			if (e.getCause() != null) {
				log.error("内部エラーが発生しました。", e.getCause());
			} else {
				log.error(e.getMessage());
			}
		} catch (Throwable e) {
			log.error("内部エラーが発生しました。", e);
		} finally {
			if (context != null && context.started) {
				context.end();
			}
			log.info("XGCrawler を終了します.");
		}
	}

	private static void updateSkillNote(Context context,
			Map<String, Result> results, boolean synchronizeCompletely) {
		if (context.config.canUpdateOnlySkillTarget()) {
			List<Result> resultList = new ArrayList<Result>(results.values());
			for (ListIterator<Result> litr = resultList.listIterator(); litr
					.hasNext();) {
				Result next = litr.next();
				if (next.getSkillTargetDetail() == null) {
					litr.remove();
				}
			}
			Collections.sort(resultList);
			List<Result> newMusicList = new ArrayList<Result>();
			List<Result> oldMusicList = new ArrayList<Result>();
			for (Result result : resultList) {
				if (result.music.isNew) {
					newMusicList.add(result);
				} else {
					oldMusicList.add(result);
				}
			}
			for (Result result : newMusicList.subList(0,
					newMusicList.size() < 25 ? newMusicList.size() : 25)) {
				SkillNoteHelper.updateSkillNote(context.skillnote, result,
						synchronizeCompletely,
						context.config.canUpdateFullcombo(),
						context.config.canAddSkillInfoToComment());
			}
			for (Result result : oldMusicList.subList(0,
					oldMusicList.size() < 25 ? oldMusicList.size() : 25)) {
				SkillNoteHelper.updateSkillNote(context.skillnote, result,
						synchronizeCompletely,
						context.config.canUpdateFullcombo(),
						context.config.canAddSkillInfoToComment());
			}
		} else {
			for (Entry<String, Result> _result : results.entrySet()) {
				Result result = _result.getValue();
				SkillNoteHelper.updateSkillNote(context.skillnote, result,
						synchronizeCompletely,
						context.config.canUpdateFullcombo(),
						context.config.canAddSkillInfoToComment());
			}
		}
	}
}
