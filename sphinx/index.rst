.. XGCrawler documentation master file, created by
   sphinx-quickstart on Wed Apr 30 12:43:16 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. title:: XGCrawler 配布サイト

=====================================
XGCrawler 配布サイト
=====================================

.. _about:

概要
====

eAMUSEMENT サイトから GITADORA OverDrive GuitarFreaks & DrumMania の達成率データを読み込み、スキル帳への自動更新を行なうツールです。


.. _feature:

特徴
====

- 更新処理を Java のみで実行するため Excel や IE を必要としません
- Java が導入されていれば Windows だけではなく Mac OS X や Linux でも動作します (WindowsXP/7/8/8.1, Mac OS X 10.8, Ubuntu 12.04 で動作確認済み)
- 判りやすいユーザーインターフェースとかはありません(設定ファイル ＋ CUI)


.. _download:

ダウンロード
============

バージョンアップの際は更新履歴内の前バージョンからの移行ガイドをお読みください。

.. raw:: html

    <a class="btn btn-primary" href="http://suitap.iceextra.org/xgcrawler/archives/XGCrawler-0.0.8-rev5.zip">ver 0.0.8-rev5 (2015/03/28)</a>
    <a class="btn btn-warning" href="http://suitap.iceextra.org/xgcrawler/archives/XGCrawler-0.0.8-rev4.zip">ver 0.0.8-rev4 (2014/06/22)</a>
    <a class="btn btn-warning" href="http://suitap.iceextra.org/xgcrawler/archives/XGCrawler-0.0.8-rev4p.zip">ver 0.0.8-rev4p (2014/06/22)</a>
    <a class="btn btn-warning" href="http://suitap.iceextra.org/xgcrawler/archives/XGCrawler-0.0.7-rev4.zip">ver 0.0.7-rev4 (2014/06/22)</a>

通常は ver 0.0.8-rev4 もしくは ver 0.0.8-rev5 をご利用ください。

| ver 0.0.8-rev4p: GITADORA OverDrive 用, 画面にデータ取得状況など進捗を出力します(機能のテスト中です)
| ver 0.0.7-rev4: 無印 GITADORA 用, 機能は ver 0.0.7 と一緒です

.. _history:

更新履歴
========

* | ver 0.0.8-rev5 (2015/03/28)
  | GITADORA OverDrive 対応版
  |   スキル対象以外の譜面・難易度をスキル帳のコメント欄に出力するためのオプション: AddSkillInfoToComment を追加しました。
  |   Y にすると、既に入力されているコメント内容は出来るだけ壊さないように更新します。
  |   また、スキル対象譜面とスキル差が10pt以内の譜面にはアスタリスク(*)が付きます。
  |   (対象の付け替えが可能かどうかが判断しやすくなります)
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.8-rev4 のファイルに上書きするだけで結構です。

* | ver 0.0.8-rev4 (2014/06/22)
  | GITADORA OverDrive 対応版
  |   ver 0.0.8-rev3 での修正が不完全だったためスキル帳の曲コメント欄を正しく更新出来なくなっていた問題を修正しました。
  |   無印 GITADORA のスキル帳更新には ver 0.0.7-rev4 を利用してください。
  |   進捗を画面に出力するバージョン(こちらはテスト中)として ver 0.0.8-rev4p を用意しています。
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.8-rev3 のファイルに上書きするだけで結構です。

* | ver 0.0.8-rev3 (2014/06/21)
  | GITADORA OverDrive 対応版
  |   スキル帳サイトの文字コードが Shift-JIS から UTF-8 に変更されたことに伴い、
  |   スキル帳サイトへのログイン処理が正常に動かなくなっていた問題を修正しました。
  |   無印 GITADORA のスキル帳更新には ver 0.0.7-rev3 を利用してください。
  |   進捗を画面に出力するバージョン(こちらはテスト中)として ver 0.0.8-rev3p を用意しています。
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.8 のファイルに上書きするだけで結構です。

.. raw:: html

    <input class="btn btn-info" type="button" id="history_button" value="過去の履歴を表示" />

* | ver 0.0.8 (2014/03/09)
  | GITADORA OverDrive 対応版
  |   OverDrive に対応しました、このバージョンでは無印 GITADORA のスキル帳のデータ更新は出来ません。
  |   無印 GITADORA 用のスキル帳を更新したい場合は ver 0.0.7-rev2 を利用してください。
  |   また、「起動後に黒い画面が出るだけで本当に動いてるのかどうか判らんぞーー」という声が多いので、
  |   データ取得状況など進捗を画面に出力するバージョン: ver 0.0.8-rev2 を用意しています。
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.7のファイルに上書きした上で、GITADORA OverDrive 用のスキル帳 ID / パスワードを config.xml に記載してください。

* | ver 0.0.7 (2014/02/17)
  | GITADORA 対応版
  |   プレーデータ抽出ログ(resultlog.txt)に「クリア回数」と「プレー回数」を出力するようにしました。
  |   パイプ文字区切りで「曲名・パート＋譜面(ADV-GとかMAS-Dとか)・達成率・スキルポイント・MAXCOMBO・フルコンマーク・スコア・クリア回数・プレー回数」の順に並んでますので、Excelなどで区切り文字に"|"を指定＆ソートしてみると楽しいと思います。
  |   そろそろバージョンアップなので「この曲はxxx回もプレイしたんだなぁ」みたいな振り返りをしたいな、と思いまして
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.6のファイルに上書きするだけで結構です。

* | ver 0.0.6 (2013/09/01)
  | GITADORA 対応版
  |   †渚の小悪魔ラヴリィ～レイディオ† (GITADORA ver.) が eAMUSEMENT サイトとスキル帳とで表記が異なるため正常に更新出来ない問題に対応
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.6-preview ～ preview5のファイルに上書きするだけで結構です。

* | ver 0.0.6-preview5 (2013/03/14)
  | GITADORA 対応テスト版
  |   プレーデータサイトからGF側データが取得できなくなっていた不具合を修正
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.6-preview ～ preview4のファイルに上書きするだけで結構です。

* | ver 0.0.6-preview4 (2013/03/06)
  | GITADORA 対応テスト版
  |   達成率がNOの難易度データがある場合にスキル帳の更新がおかしくなることがある不具合を修正
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.6-preview ～ preview3のファイルに上書きするだけで結構です。

* | ver 0.0.6-preview3 (2013/03/01)
  | GITADORA 対応テスト版
  |   GITADORA ではプレーデータが 1 曲ごとでしか取得できなくなっているので、以前と比べて実行時間が大変遅くなっています。
  |   (GF/DM 両方更新で 15 ～ 20 分前後、片方のみ更新で 7 ～ 10 分前後)
  |   取得中のデータを随時 resultlog.txt に出力するようにしているので、
  |   そのファイルの最後の行を見て頂けると「今どのあたりまで処理してるか」が判るかな、と。
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.5-rev3のファイルに上書きして、スキル帳のユーザー ID を変更 ＆ config.xml の SynchronizeCompletely を Y にしてください。
  |   (SynchronizeCompletely オプションの変更は必須ではありませんが、preview1,2 のバグによる更新不整合をツールによって直せるようになるハズなので推奨しています)

* | ver 0.0.5-rev3 (2012/12/19)
  | WILD CREATRURESのスキル帳データが更新出来ない問題に対応
  | ※2012/12/10頃より WILD CREATURES が WILD CREATRURES と表記の修正が行なわれ、GATEとスキル帳サイトとで曲名が一致しなくなったため
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.5-rev2のファイルに上書きするだけで結構です。

* | ver 0.0.5-rev2 (2012/04/04)
  | GATEとスキル帳とで達成率が同じ＆FCマークの有無が異なる場合にFCマークの更新処理が行なわれないバグを修正
  |
  | 前バージョンからの移行ガイド
  |   ver 0.0.5のファイルに上書きするだけで結構です。

* | ver 0.0.5 (2012/04/04)
  | FCマークをスキル帳に反映させるためのオプションを追加
  |   ※このオプションを有効にすると、GATEへのアクセス回数が極端に増え、
  |   処理時間も比例して長くなります(プレイ状況によりますが数倍から数十倍)。常用は控えてください。
  |
  | 前バージョンからの移行ガイド
  |   このバージョンにて、config.xml で指定可能な設定項目が 1 項目増えています。
  |   配布ファイル内の config-template.xml から config.xml へコピーするなどしてください。

* | ver 0.0.4 (2012/03/30)
  | 達成率が GATE > スキル帳 となる場合でも更新させるためのオプションを追加
  |
  | 前バージョンからの移行ガイド
  |   このバージョンにて、config.xml で指定可能な設定項目が 1 項目増えています。
  |   配布ファイル内の config-template.xml から config.xml へコピーするなどしてください。

* | ver 0.0.3 (2012/03/29)
  | 全曲更新するかスキル対象曲のみ更新するかを設定可能にした
  | GF/DMどちらかのみ、もしくは両方更新するかを設定可能にした
  | 更新対象となった曲及びパートを実行ログ(crawlerlog.txt)に書き出すようにする
  |
  | 前バージョンからの移行ガイド
  |   このバージョンにて、config.xml で指定可能な設定項目が 3 項目増えています。
  |   配布ファイル内の config-template.xml から config.xml へコピーするなどしてください。

* | ver 0.0.2 (2012/03/26)
  | スキル帳のランク欄がEXCとなっている(達成率100%の)曲が存在する場合、
  | スキル帳のデータを正しく解釈できずに更新処理を行なわないまま終了してしまうバグを修正
  | ツール名称を EAGateCrawler から XGCrawler に変更
  | zipファイルに同梱している設定ファイル: config.xml を config-template.xml に名前変更
  | (バージョンアップの際に誤って上書きしないようにする)

* | ver 0.0.1 (2012/03/22)
  | 初版


.. _howtouse:

使い方
======


.. _initandrun:

初期設定から起動まで
--------------------

例としてWindowsでの使い方を記載します

1. zipファイルを適当な場所に展開
2. 展開先ディレクトリの config-template.xml を適当なテキストエディタで開く(Windows付属のメモ帳でもOK)
3. | EAGateUsername: eAMUSEMENTのログインID
   | EAGatePassword: eAMUSEMENTのパスワード
   | SkillNoteUsername: スキル帳のGITADORA用ユーザーID
   | SkillNotePassword: スキル帳のGITADORA用パスワード
   | を記入して保存
4. | 上記で編集した config-template.xml のファイル名を config.xml に変更 :doc:`way_to_rename`
   | (config-template.xml はあくまで設定ファイルの雛形であり、実際にツールが読み込むファイルは config.xml です)
5. run.batをダブルクリックで実行する
6. 黒いウインドウが開きます
7. 「続行するには何かキーを押してください」というメッセージが出たら処理完了、適当にキーを押したら終了します

| 2回目以降の実行の際は、手順5.(run.batダブルクリック)からでおっけーです
| 実行の際は Java (Version 6 以上) が必要なので http://www.java.com/ja/ から入手＆インストールしておいてください
| (上記サイトで最新版として案内されているバージョンならどのバージョンでもOKです)

Windows以外のOSでは `jarファイルをダブルクリック` もしくは `シェル/ターミナルより java -jar XGCrawler.jar` で起動できます


.. _tips:

動作について
------------

| スキル帳の難易度表を元にスキルを計算しています、この表に登録されていない曲やパートのデータは無視されます
| たまに更新漏れが起こることがありますが、その際は再度実行しなおしてみてください(GATEが不調の場合は多発します)


.. _configxml:

設定ファイル config.xml について
--------------------------------

.. image:: _static/config1.png

zipファイルから展開した状態では何の設定もされていない状態なので…

.. image:: _static/config2.png

> と < の間にログインIDなどを入れてあげてください

.. list-table:: config.xml設定項目一覧
  :widths: 10 30 10
  :header-rows: 1

  * - 設定項目
    - 意味
    - 備考
  * - EAGateUsername
    - eAMUSEMENTへのログインに使用するユーザーID
    - 必須
  * - EAGatePassword
    - eAMUSEMENTへのログインに使用するパスワード
    - 必須
  * - SkillNoteUsername
    - スキル帳へのログインに使用するユーザーID
    - 必須
  * - SkillNotePassword
    - スキル帳へのログインに使用するパスワード
    - 必須
  * - UpdateGuitarSkill
    - | ギター側スキルを更新するかどうか
      | Y: 更新する N: 更新しない
    - | ver 0.0.3で追加
      | 空欄もしくはファイル内に行がない場合: Y
  * - UpdateDrumSkill
    - | ドラム側スキルを更新するかどうか
      | Y: 更新する N: 更新しない
    - | ver 0.0.3で追加
      | 空欄もしくはファイル内に行がない場合: Y
  * - UpdateOnlySkillTarget
    - | 更新対象をスキル対象曲(新曲25曲＋旧曲25曲)のみにするかどうか
      | Y: スキル対象曲のみ更新 N: 全曲更新
    - | ver 0.0.3で追加
      | 空欄もしくはファイル内に行がない場合: N
  * - SynchronizeCompletely
    - | 達成率が GATE > スキル帳 となる場合でも更新するかどうか
      | Y: 更新する N: 更新しない
      | ver 0.0.6から、このオプションはデフォルト値がNからYに変更になりました。
      | このオプションをNにしていると、
      | 「XGCrawlerの不具合などでスキル帳におかしな更新がされる
      | →不具合修正版を動かす
      | →(本来は正常な値で更新されるべきだが)達成率が GATE > スキル帳 となっているので更新対象外となってしまう」可能性があり、
      | 「ツールによるデータ自動修復の機会をなくすのはもったいないなぁ」という判断によるものです。
    - | ver 0.0.4で追加
      | 空欄もしくはファイル内に行がない場合: Y
  * - UpdateFullcombo
    - | FCマークをスキル帳に反映させるかどうか
      | Y: 反映させる N: 反映させない
      | ※このオプションを有効(Y)にすると、GATEへのアクセス回数が極端に増え、処理時間も比例して長くなります。(プレイ状況によりますが数倍から数十倍)常用は控えてください。
    - | ver 0.0.5で追加
      | 空欄もしくはファイル内に行がない場合: N
  * - DeleteAll
    - | データ全削除モードで動作させるかどうか
      | Y: 動作させる N: 動作させない
      | ※このオプションを有効(Y)にすると、スキル帳の入力データを全て削除するモードで動作します。
      | (Guitar/Drum共に削除、コメント含めて行ごと消します)
      | 誤ってYに設定しないように、デフォルトの設定ファイルには記載していません。
      | このモードを有効にする場合は、手動で <entry key="DeleteAll">Y</entry> を追加してください。
      | 削除する前は各自バックアップを取っておくことを推奨します。
    - | ver 0.0.3で追加
      | 空欄もしくはファイル内に行がない場合: N
  * - AddSkillInfoToComment
    - | スキル対象以外の譜面・難易度をスキル帳のコメント欄に出力するかどうか
      | Y: 出力する N: 出力しない
      | 既に入力されているコメント内容は出来るだけ壊さないように更新します。
      | また、スキル対象譜面とスキル差が10pt以内の譜面にはアスタリスク(*)が付きます。
      | (対象の付け替えが可能かどうかが判断しやすくなります)
    - | ver 0.0.8-rev5 で追加
      | 空欄もしくはファイル内に行がない場合: N
  * - ProxyHost
    - Proxyホスト名
    - 任意、通常は空欄で
  * - ProxyPort
    - Proxyポート
    - 任意、通常は空欄で


.. _logfiles:

ログファイルについて
--------------------

.. list-table::
  :widths: 10 40
  :header-rows: 1

  * - ファイル名
    - 内容
  * - crawlerlog.txt
    - config.xmlで指定した設定＋更新対象となった曲が出力されます。
  * - resultlog.txt
    - | プレーデータから取得したデータが出力されます。
      | (曲名・パート＋譜面(ADV-GとかMAS-Dとか)・達成率・スキルポイント・MAXCOMBO・フルコンマーク・スコア・クリア回数・プレー回数)


.. _troubleshooting:

おかしいな？と思ったら
======================

* 実行すると「eAMUSEMENT にログインできませんでした.」というエラーが出る
    | eAMUSEMENT のユーザー名もしくはパスワードが誤っています。
    | config.xml の EAGateUsername 欄と EAGatePassword 欄を再度確認してください。

* 実行すると「GuitarFreaks & DrumMania Skill Simulator にログインできませんでした.」というエラーが出る
    | スキル帳のユーザー名もしくはパスワードが誤っています。
    | config.xml の SkillNoteUsername 欄と SkillNotePassword 欄を再度確認してください。

* 実行すると「設定ファイルが存在しません。」というエラーが出る
    | 設定ファイルのファイル名変更 ( config-template.xml → config.xml ) がされていることを確認してください。
    | :ref:`howtouse` の手順 4 もしくは :doc:`way_to_rename` を参照してください。

* run.bat を実行したけど「指定されたファイルが見つかりません」というエラーが出る
    | Javaがインストールされていないかも知れません。
    | Javaがインストール済みである場合、run.batの9行目の
    |     `XGCrawler.jar`
    | と書かれている行を、
    |     `java -jar XGCrawler.jar`
    | もしくは
    |     `C:\\Windows\\System32\\java.exe -jar XGCrawler.jar`
    | もしくは
    |     `"C:\\Program Files\\Java\\jre7\\bin\\java.exe" -jar XGCrawler.jar`
    | と書き換えて実行してみてください。(コピー＆ペーストで貼り付けると楽です)
    | ファイルの編集は run.bat を右クリック→「編集」で出来ます(メモ帳が開きます)。

* run.bat を実行すると「プレーデータが正常に読み込めません」といったメッセージが出る
    | プレーデータのページから正しく達成率データが読み込みできなかった時に表示されます。
    | 正しく読み込みできなかった曲については更新処理をせずに次の曲の処理を続けるようになっていますので、一旦実行が終わった後にもう一度実行すれば良いです。
    | このメッセージは、「XGCrawlerを実行しつつプレーデータをブラウザもしくはGITADORAアプリで見てる」場合に必ず出てきます。
    | XGCrawler実行中は、プレーデータを参照しないようにして下さい。

* 「内部エラーが発生しました。」というエラーが出る
    | だいたいは「設定ファイルの中身がおかしくなっていてXGCrawlerが設定を読み込めなかった」もしくは「(一時的な)通信エラーが発生した」場合に発生します。
    | もう一度XGCrawlerを実行してみてその結果ちゃんと動作したのであれば、恐らく通信エラーによる内部エラーだと思われますので、気にしないでください。
    | 「設定ファイルに問題はないのに何度も内部エラーが出る！」といった場合は、 :ref:`contact` まで連絡ください。


.. _misc:

そのほか
========

.. _security:

このツールのパスワードの取り扱いについて
----------------------------------------

| このツールを実行するにあたり、eAMUSEMENT サイトログイン用の KONAMI ID＋パスワードとスキル帳サイトのユーザー名＋パスワードを必要とします。
| これらの情報は、以下の処理以外に使用することはありません。

* KONAMI ID＋パスワード: eAMUSEMENT サイトへログインし、GITADORA プレーデータを取得する
* スキル帳ユーザー名＋パスワード: スキル帳サイトへログインし、スキル帳データを更新する

| 要は作者がパスワードを盗んで不正アクセスに利用したり利用者以外の第三者に流したりはしてません、ということです。
| (p.eagate.573.jp および xv-s.heteml.jp 以外との通信は一切しません)
| ソースコードも公開していますので、ソースコードが読める方は :ref:`sources` にて確認出来ます。
| config.xml 自体が盗まれるといった懸念がある場合は、(Windows 限定ですが) 後述の XGCrawlerUI の利用を検討してみてください。
| XGCrawlerUI は config.xml の暗号化機能を備えています。詳しくは :ref:`xgcrawlerui` を参照してください。


.. _xgcrawlerui:

XGCrawlerUIについて
-------------------

| XGCrawlerは設定ファイル config.xml を手で編集しなければならないのが面倒ですね！
| そんなあなたへ。config.xml の編集が画面上で簡単に行える素敵ツール、あります

詳しくは: `GuiterFreaksXG DrummaniaXGスキルシュミレーター更新用UI - ふなちゅんねっと <http://funachun.net/skill.html>`_


.. _roadmap:

今後の予定
----------

* スキル対象曲のみ更新するor全曲更新するかを設定可能にする → ver 0.0.3で対応済
* GDいずれかのみ、もしくは両方とも更新するかを設定可能にする → ver 0.0.3で対応済
* 更新対象となった曲のリストを利用者が把握できるようにする → ver 0.0.3で対応済
* フルコンボ情報の取得及び反映 → ver 0.0.5で対応済
* すまほ・・・？


.. _contact:

連絡先
------

| 作者は TwitterID: `@suitap <http://twitter.com/suitap>`_ です
| 質問・要望・バグ報告などは今のところTwitter上でのみ受け付けております
| たまに "XGCrawler" でTweet検索してるので、バグを見つけたら「こういう条件の時にバグるからさっさとXGCrawler直せハゲ」みたいなツイートしとけば数日後に直るかも知れません(直らないかも知れません:-P)

.. raw:: html

    <a href="https://twitter.com/intent/tweet?screen_name=suitap" class="twitter-mention-button" data-lang="ja" data-size="large">Tweet to @suitap</a>

| このサイトへのリンクはご自由にどぞー(報告連絡相談は特に不要です、したい人はどうぞ)
| ただし再配布は勘弁な


.. _sources:

ソースコードについて
--------------------

`bitbucketで公開しています。 <https://bitbucket.org/suitap/xgcrawler>`_


.. raw:: html

    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <script type="text/javascript">
      $(function() {
        $("#history_button").click(function () {
          $("#history li:gt(1)").toggle();
        }).click();
      });
    </script>
