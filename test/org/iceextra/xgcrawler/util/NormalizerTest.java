package org.iceextra.xgcrawler.util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class NormalizerTest extends Normalizer {

	@Test
	public void testNormalizeMusicName() {
		assertThat(normalizeMusicName("サムライハート （Some Like It Hot!!）"),
				is(equalTo("サムライハート(somelikeithot!!)")));
		assertThat(normalizeMusicName("サムライハート(Some Like It Hot!!)"),
				is(equalTo("サムライハート(somelikeithot!!)")));
		assertThat(normalizeMusicName("真実と闘争 TRUTH AND STRUGGLE"),
				is(equalTo("真実と闘争truthandstruggle")));
		assertThat(normalizeMusicName("太陽〜T・A・I・Y・O〜(ANOTHER SENSE EDITION)"),
				is(equalTo("太陽〜t・a・i・y・o〜(anothersenseedition)")));
		assertThat(normalizeMusicName("You can't do it if you try"),
				is(equalTo("youcan'tdoitifyoutry")));
		assertThat(normalizeMusicName("WILD CREATRURES"),
				is(equalTo("wildcreatrures")));
		assertThat(normalizeMusicName("WILD CREATURES"),
				is(equalTo("wildcreatrures")));
		assertThat(normalizeMusicName("†渚の小悪魔ラヴリィ～レイディオ†"),
				is(equalTo("†渚の小悪魔ラヴリィ~レイディオ†(gitadoraver.)")));
		assertThat(normalizeMusicName("†渚の小悪魔ラヴリィ～レイディオ† (GITADORA ver.)"),
				is(equalTo("†渚の小悪魔ラヴリィ~レイディオ†(gitadoraver.)")));
		assertThat(normalizeMusicName("ZЁNITH"), is(equalTo("zenith")));
		assertThat(normalizeMusicName("RЁVOLUTIΦN"), is(equalTo("revolution")));
		assertThat(normalizeMusicName(""), is(equalTo("")));
	}

}
