package org.iceextra.xgcrawler.helper;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.iceextra.xgcrawler.exception.CrawlerException;
import org.iceextra.xgcrawler.internal.Config;
import org.iceextra.xgcrawler.internal.HttpClientWrapper;
import org.iceextra.xgcrawler.internal.RequestParams;
import org.iceextra.xgcrawler.model.Achievement;
import org.iceextra.xgcrawler.model.Difficulty;
import org.iceextra.xgcrawler.model.Music;
import org.iceextra.xgcrawler.model.Result;
import org.iceextra.xgcrawler.model.ResultDetail;
import org.iceextra.xgcrawler.model.SkillNoteResult;
import org.iceextra.xgcrawler.model.types.Level;
import org.iceextra.xgcrawler.model.types.Rank;
import org.iceextra.xgcrawler.model.types.Type;
import org.iceextra.xgcrawler.util.Normalizer;

public class SkillNoteHelper {

	private static final Log log = LogFactory.getLog(SkillNoteHelper.class);

	public static void loginSkillNote(HttpClientWrapper client,
			String username, String password) {
		client.GET(SKILLNOTE_LOGIN_URL);
		String contents = client.POST(SKILLNOTE_LOGIN_URL, new RequestParams(
				"id", username).add("passwd", password));
		if (!isLoginSuccessful(username, contents)) {
			log.error("GuitarFreaks & DrumMania Skill Simulator にログインできませんでした.");
			throw new CrawlerException("failed to login on skill simulator.");
		}
		log.info("GuitarFreaks & DrumMania Skill Simulator にログイン完了.");
	}

	private static boolean isLoginSuccessful(String username, String contents) {
		if (StringUtils.isBlank(contents)) {
			return false;
		}
		return contents.contains("ユーザID : " + username);
	}

	public static void logoutSkillNote(HttpClientWrapper client) {
		client.GET(SKILLNOTE_LOGOUT_URL);
		log.info("GuitarFreaks & DrumMania Skill Simulator からログアウトしました.");
	}

	public static Map<String, Music> loadMusics(HttpClientWrapper client) {

		Map<String, Music> list = new LinkedHashMap<String, Music>();

		for (String s : new String[] { "new", "old" }) {
			String data = client.GET(SKILLNOTE_DIFFICULTYLIST_URL + s);
			final boolean isNew = "new".equals(s);

			Matcher skillListMatcher = skillListPattern.matcher(data);
			while (skillListMatcher.find()) {

				String musicName = skillListMatcher.group(1);
				Music music = new Music();
				music.skillnoteName = musicName;
				music.name = Normalizer.normalizeMusicName(musicName);
				music.isNew = isNew;
				list.put(music.name, music);

				Set<Difficulty> d = music.difficulties;

				Matcher m;
				m = skillValuePattern.matcher(skillListMatcher.group(2));
				if (m.find()) {
					d.add(new Difficulty(music, Level.BSC, Type.D,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(3));
				if (m.find()) {
					d.add(new Difficulty(music, Level.ADV, Type.D,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(4));
				if (m.find()) {
					d.add(new Difficulty(music, Level.EXT, Type.D,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(5));
				if (m.find()) {
					d.add(new Difficulty(music, Level.MAS, Type.D,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(6));
				if (m.find()) {
					d.add(new Difficulty(music, Level.BSC, Type.G,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(7));
				if (m.find()) {
					d.add(new Difficulty(music, Level.ADV, Type.G,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(8));
				if (m.find()) {
					d.add(new Difficulty(music, Level.EXT, Type.G,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(9));
				if (m.find()) {
					d.add(new Difficulty(music, Level.MAS, Type.G,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(10));
				if (m.find()) {
					d.add(new Difficulty(music, Level.BSC, Type.B,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(11));
				if (m.find()) {
					d.add(new Difficulty(music, Level.ADV, Type.B,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(12));
				if (m.find()) {
					d.add(new Difficulty(music, Level.EXT, Type.B,
							new BigDecimal(m.group(1))));
				}
				m = skillValuePattern.matcher(skillListMatcher.group(13));
				if (m.find()) {
					d.add(new Difficulty(music, Level.MAS, Type.B,
							new BigDecimal(m.group(1))));
				}
			}
		}

		Map<String, String> skillNoteMusicIds = new LinkedHashMap<String, String>();
		setSkillEditModeToGf(client);
		addSkillNoteMusic(client.GET(SKILLNOTE_GF_MUSIC_LIST_URL),
				skillNoteMusicIds);
		setSkillEditModeToDm(client);
		addSkillNoteMusic(client.GET(SKILLNOTE_DM_MUSIC_LIST_URL),
				skillNoteMusicIds);

		for (Entry<String, Music> entry : list.entrySet()) {
			if (skillNoteMusicIds.containsKey(entry.getValue().skillnoteName)) {
				entry.getValue().skillnoteId = skillNoteMusicIds.get(entry
						.getValue().skillnoteName);
			}
		}

		log.info("Skill Simulator から曲の難易度データを読み込みました.");
		return list;
	}

	public static void mergeGfSkillNoteResult(HttpClientWrapper client,
			Config config, Map<String, Result> results) {
		String data = client.GET(SKILLNOTE_GF_SKILL_LIST_URL
				+ config.getSkillNoteUsername());
		Matcher m = skillGfResultPattern.matcher(data);
		while (m.find()) {
			for (Entry<String, Result> _result : results.entrySet()) {
				Result result = _result.getValue();
				if (result.music.skillnoteName.equals(m.group(1))) {
					Level level = Level.valueOf(m.group(3));
					Type type = Type.valueOf(m.group(4));
					Rank rank = Rank.valueOf(m.group(7));
					Achievement achivement = new Achievement(m.group(5));
					BigDecimal skillpoints = new BigDecimal(m.group(6));
					boolean isFullcombo = StringUtils.isNotBlank(m.group(8));
					boolean isRandom = m.group(9).contains("RAN");
					boolean isSuperRandom = m.group(9).contains("SRA");
					boolean isLeft = m.group(9).contains("LEF");
					String comment = m.group(10);
					result.skillNoteResult = new SkillNoteResult(level, type,
							rank, achivement, skillpoints, isFullcombo,
							isRandom, isSuperRandom, isLeft, comment);
				}
			}
		}
	}

	public static void mergeDmSkillNoteResult(HttpClientWrapper client,
			Config config, Map<String, Result> results) {
		String data = client.GET(SKILLNOTE_DM_SKILL_LIST_URL
				+ config.getSkillNoteUsername());
		Matcher m = skillDmResultPattern.matcher(data);
		while (m.find()) {
			for (Entry<String, Result> _result : results.entrySet()) {
				Result result = _result.getValue();
				if (result.music.skillnoteName.equals(m.group(1))) {
					Level level = Level.valueOf(m.group(3));
					Type type = Type.D;
					Rank rank = Rank.valueOf(m.group(7));
					Achievement achivement = new Achievement(m.group(5));
					BigDecimal skillpoints = new BigDecimal(m.group(6));
					boolean isFullcombo = StringUtils.isNotBlank(m.group(8));
					String comment = m.group(9);
					result.skillNoteResult = new SkillNoteResult(level, type,
							rank, achivement, skillpoints, isFullcombo, false,
							false, false, comment);
				}
			}
		}
	}

	public static void addSkillNoteMusic(String source, Map<String, String> map) {
		Matcher m = skillnoteMusicListPattern.matcher(source);
		while (m.find()) {
			map.put(m.group(2), m.group(1));
		}
	}

	public static void setSkillEditModeToGf(HttpClientWrapper client) {
		client.GET(SKILLNOTE_GFMODE_URL);
	}

	public static void setSkillEditModeToDm(HttpClientWrapper client) {
		client.GET(SKILLNOTE_DMMODE_URL);
	}

	public static void updateSkillNote(HttpClientWrapper client, Result result,
			boolean synchronizeCompletely, boolean canUpdateFullcombo) {
		updateSkillNote(client, result, synchronizeCompletely,
				canUpdateFullcombo, false);
	}

	/**
	 * スキルノートを更新する
	 * 
	 * @param client
	 * @param result
	 * @param synchronizeCompletely
	 * @param canUpdateFullcombo
	 * @param addSkillInfoToComment
	 *            コメント欄にスキル情報を追加するなら true
	 */
	public static void updateSkillNote(HttpClientWrapper client, Result result,
			boolean synchronizeCompletely, boolean canUpdateFullcombo,
			boolean addSkillInfoToComment) {

		ResultDetail targetDetail = result.getSkillTargetDetail();
		if (targetDetail == null) {
			return;
		}

		String comment = getCommentWithSkills(result);
		if (!addSkillInfoToComment) {
			comment = getOriginalComment(comment);
		}

		RequestParams params = new RequestParams();
		params.add("music_id", result.music.skillnoteId);
		params.add("act", "0");
		if (targetDetail.type == Type.D) {
			params.add("seq_type", targetDetail.level);
		} else {
			params.add("seq_type", targetDetail.level + "(" + targetDetail.type
					+ ")");
		}
		params.add("acv", targetDetail.achievements.value.toPlainString());

		if (result.skillNoteResult != null) {
			int comparedSkillPoints = result.skillNoteResult.skillpoints
					.compareTo(targetDetail.getSkillPoints());
			boolean fullcomboChanged = canUpdateFullcombo
					&& result.skillNoteResult.isFullcombo != targetDetail.isFullcombo;
			final boolean commentChanged = !result.skillNoteResult.comment
					.equals(comment);
			final boolean needsUpdate = fullcomboChanged
					|| commentChanged
					|| (synchronizeCompletely ? (comparedSkillPoints != 0)
							: (comparedSkillPoints < 0));
			if (!needsUpdate) {
				return;
			}

			if (!canUpdateFullcombo) {
				params.add("rank", result.skillNoteResult.isFullcombo ? "FC"
						: "NN");
			} else {
				if (targetDetail.isFullcombo == null
						|| !targetDetail.isFullcombo) {
					params.add("rank", "NN");
				} else {
					params.add("rank", "FC");
				}
			}

			if (targetDetail.type == Type.G || targetDetail.type == Type.B) {
				if (result.skillNoteResult.isRandom) {
					params.add("option[]", "RAN");
				}
				if (result.skillNoteResult.isSuperRandom) {
					params.add("option[]", "SRA");
				}
				if (result.skillNoteResult.isLeft) {
					params.add("option[]", "LEFT");
				}
			}
			params.add("comment", comment);
		} else {
			if (!canUpdateFullcombo) {
				params.add("rank", "NN");
			} else {
				if (targetDetail.isFullcombo == null
						|| !targetDetail.isFullcombo) {
					params.add("rank", "NN");
				} else {
					params.add("rank", "FC");
				}
			}
			params.add("comment", comment);
		}

		log.debug(client.POST(SKILLNOTE_SKILLEDIT_BASE_URL, params));
		log.info(result.music.eagateName + "(" + targetDetail.level + "-"
				+ targetDetail.type + ") のスキル情報を更新しました.");
	}

	/**
	 * コメント欄の文字列からスキル値情報を取り除いた文字列を取得する
	 * 
	 * @param comment
	 *            コメント欄の文字列
	 * @return スキル値情報を取り除いた文字列
	 */
	protected static String getOriginalComment(final String comment) {
		if (comment == null) {
			return "";
		}
		if (comment.length() <= 0) {
			return "";
		}

		final String separaterRegex = ":";
		final String levelRegex = "(?:" + StringUtils.join(Level.values(), "|")
				+ ")" + "(?:\\([BG]\\))?" + separaterRegex;
		final String starRegex = "\\*?";
		final String decimalRegex = "\\d+(?:\\.\\d+)?";
		final String achievementsRegex = decimalRegex + "%";
		final String skillPointsRegex = "\\(" + decimalRegex + "\\)";
		final String skillStringRegex = starRegex + levelRegex
				+ achievementsRegex + skillPointsRegex;
		final String skillStringsRegex = "(?:" + skillStringRegex + " *)*";
		final String commentRegex = "^((?:(?!" + starRegex + levelRegex
				+ ").)*)" + skillStringsRegex + "(.*?)$";

		final Pattern pattern = Pattern.compile(commentRegex);

		final Matcher matcher = pattern.matcher(comment);

		if (matcher.find()) {
			final String before = "" + matcher.group(1);
			final String after = "" + matcher.group(2);
			return (before.trim() + " " + after.trim()).trim();
		}
		return "";
	}

	/**
	 * ターゲットのスキル値と比較対象のスキル値の差が diff 以下ならスキル値の更新が期待できるという扱いにする
	 * 
	 * @param targetLevel
	 *            ターゲットの {@link Level}
	 * @param targetSkillPoints
	 *            ターゲットのスキル値
	 * @param level
	 *            比較対象の {@link Level}
	 * @param skillPoints
	 *            比較対象のスキル値
	 * @param diff
	 *            スキル値の差
	 * @return スキル値の更新が期待できるなら true, それ以外なら false
	 */
	protected static boolean isSkillUpdateExpected(final Level targetLevel,
			final BigDecimal targetSkillPoints, final Level level,
			final BigDecimal skillPoints, final int diff) {
		if (targetLevel == null || targetSkillPoints == null || level == null
				|| skillPoints == null || diff <= 0) {
			return false;
		}
		if (level.compareTo(targetLevel) < 0) {
			// ターゲットより低いレベルは無視する
			// (同じか高いレベルの場合は比較する)
			return false;
		}
		if (targetSkillPoints.subtract(skillPoints).signum() <= 0) {
			// 一番スキル値が大きいからターゲットになっている
			// ターゲットのスキル値より大きいのはありえないので無視する
			// ターゲットと同じスキル値なら高いレベルのほうがターゲットになっているはずなので無視する
			return false;
		}
		boolean skillUpdateExpected = false;
		if (targetSkillPoints.subtract(skillPoints)
				.subtract(new BigDecimal(diff)).signum() <= 0) {
			// ターゲットのスキル値と比較対象のスキル値の差が diff 以下なら
			// スキル値の更新が期待できるという扱いにする
			skillUpdateExpected = true;
		}
		return skillUpdateExpected;
	}

	/**
	 * "BSC", "ADV(G)", "EXT(B)" といった文字列を返す
	 * 
	 * @param level
	 *            {@link Level}
	 * @param type
	 *            {@link Type}
	 * @return "BSC", "ADV(G)", "EXT(B)" といった文字列
	 */
	protected static String getSeqTypeString(final Level level, final Type type) {
		if (level == null || type == null) {
			return "";
		}
		switch (type) {
		case D:
			return "" + level;
		case G:
		case B:
			return String.format("%s(%s)", level, type);
		default:
			return "";
		}
	}

	/**
	 * "BSC-D", "ADV-G" といったキーと ResultDetail の値でマップを作成
	 * 
	 * @param details
	 *            {@link ResultDetail} の {@link Set}
	 * @return "BSC-D", "ADV-G" といったキーと ResultDetail の値で作成されたマップ
	 */
	protected static Map<String, ResultDetail> getResultDetailMap(
			final Set<ResultDetail> details) {
		final Map<String, ResultDetail> resultDetailMap = new HashMap<String, ResultDetail>();
		if (details == null) {
			return resultDetailMap;
		}
		for (final Iterator<ResultDetail> iterator = details.iterator(); iterator
				.hasNext();) {
			final ResultDetail resultDetail = iterator.next();
			final String key = getSeqTypeString(resultDetail.level,
					resultDetail.type);
			resultDetailMap.put(key, resultDetail);
		}
		return resultDetailMap;
	}

	/**
	 * ターゲット以外のスキル情報付きのコメント(比較対象のみ)を取得する
	 * 
	 * @param targetDetail
	 *            ターゲットの {@link ResultDetail}
	 * @param resultDetail
	 *            比較対象の {@link ResultDetail}
	 * @return ターゲット以外のスキル情報付きのコメント(比較対象のみ)
	 */
	protected static String getCommentWithSkill(
			final ResultDetail targetDetail, final ResultDetail resultDetail) {
		if (targetDetail == null || resultDetail == null) {
			return "";
		}
		final BigDecimal resultSkillPoints = resultDetail.getSkillPoints();
		final BigDecimal targetSkillPoints = targetDetail.getSkillPoints();
		final boolean isSkillUpdateExpected = isSkillUpdateExpected(
				targetDetail.level, targetSkillPoints, resultDetail.level,
				resultSkillPoints, 10);
		return String.format("%s%s:%s%%(%s)",
				(isSkillUpdateExpected ? "*" : ""),
				getSeqTypeString(resultDetail.level, resultDetail.type),
				resultDetail.achievements.value.toPlainString(),
				resultSkillPoints.toPlainString());
	}

	/**
	 * ターゲット以外のスキル情報付きのコメントを取得する
	 * 
	 * @param result
	 *            {@link Result}
	 * @return ターゲット以外のスキル情報付きのコメント
	 */
	protected static String getCommentWithSkills(final Result result) {
		if (result == null) {
			return "";
		}

		final ResultDetail targetDetail = result.getSkillTargetDetail();
		if (targetDetail == null) {
			return "";
		}

		final Map<String, ResultDetail> resultDetailMap = getResultDetailMap(result.details);
		String comment = "";

		// Level クラスは 難易度の低い順なので逆順にする
		final List<Level> levels = Arrays.asList(Level.values());
		Collections.reverse(levels);
		for (final Level level : levels) {
			for (final Type type : Type.values()) {
				if (level == targetDetail.level && type == targetDetail.type) {
					continue;
				}
				final String key = getSeqTypeString(level, type);
				if (!resultDetailMap.containsKey(key)) {
					continue;
				}
				comment += getCommentWithSkill(targetDetail, resultDetailMap.get(key))
						+ " ";
			}
		}

		final SkillNoteResult skillNoteResult = result.skillNoteResult;
		final String skillNoteResultComment = (skillNoteResult != null ? skillNoteResult.comment
				: "");
		return (comment.trim() + " " + getOriginalComment(skillNoteResultComment))
				.trim();
	}

	public static void deleteAllResult(HttpClientWrapper client) {
		Matcher m;
		{
			setSkillEditModeToGf(client);
			String gfmain = client.GET(SKILLNOTE_GFMAIN_URL);
			if (StringUtils.isEmpty(gfmain)) {
				return;
			}
			m = skillnoteMusicIdPattern.matcher(gfmain);
			while (m.find()) {
				String musicId = m.group(1);
				RequestParams params = new RequestParams();
				params.add("music_id", musicId);
				params.add("act", "1");
				client.POST(SKILLNOTE_SKILLEDIT_BASE_URL, params);
			}
		}
		{
			setSkillEditModeToDm(client);
			String dmmain = client.GET(SKILLNOTE_DMMAIN_URL);
			if (StringUtils.isEmpty(dmmain)) {
				return;
			}
			m = skillnoteMusicIdPattern.matcher(dmmain);
			while (m.find()) {
				String musicId = m.group(1);
				RequestParams params = new RequestParams();
				params.add("music_id", musicId);
				params.add("act", "1");
				client.POST(SKILLNOTE_SKILLEDIT_BASE_URL, params);
			}
		}
	}

	private static final Pattern skillListPattern = Pattern
			.compile(
					"<tr>.*?<td id=etc class=l>(.+?)</td>.*?"
							+ "<td id=nov>(.+?)</td>.*?<td id=reg>(.+?)</td>.*?<td id=exp>(.+?)</td>.*?<td id=mas>(.+?)</td>.*?"
							+ "<td id=nov>(.+?)</td>.*?<td id=reg>(.+?)</td>.*?<td id=exp>(.+?)</td>.*?<td id=mas>(.+?)</td>.*?"
							+ "<td id=nov>(.+?)</td>.*?<td id=reg>(.+?)</td>.*?<td id=exp>(.+?)</td>.*?<td id=mas>(.+?)</td>.*?"
							+ "<td id=etc class=l>.*?</td>.*?</tr>",
					Pattern.DOTALL);

	private static final Pattern skillGfResultPattern = Pattern.compile(
			"<tr id=r[nrem]><td id=j.*?</td>" + "<td.*?>(.*?)</td>"
					+ "<td.*?>(.*?) (ADV|BSC|EXT|MAS)(?:\\((.)\\))?</td>"
					+ "<td.*?>(.*?)%</td>" + "<td.*?>(.*?)</td>"
					+ "<td.*?>(.*?)(/FC)?</td>" + "<td.*?>(.*?)</td>"
					+ "<td.*?>(.*?)</td></tr>", Pattern.DOTALL);

	private static final Pattern skillDmResultPattern = Pattern.compile(
			"<tr id=r[nrem]><td id=j.*?</td>" + "<td.*?>(.*?)</td>"
					+ "<td.*?>(.*?) (ADV|BSC|EXT|MAS)(?:\\((.)\\))?</td>"
					+ "<td.*?>(.*?)%</td>" + "<td.*?>(.*?)</td>"
					+ "<td.*?>(.*?)(/FC)?</td>" + "<td.*?>(.*?)</td></tr>",
			Pattern.DOTALL);

	private static final Pattern skillValuePattern = Pattern
			.compile("(\\d+\\.\\d+)");

	private static final Pattern skillnoteMusicListPattern = Pattern.compile(
			"<option value='(\\d+)'>(.+?)</option>", Pattern.DOTALL);

	private static final Pattern skillnoteMusicIdPattern = Pattern.compile(
			"<a class=\"ml\" href=\"acvedit_gdod.php\\?music_id=(\\d+)\">",
			Pattern.DOTALL);

	private static final String SKILLNOTE_LOGIN_URL = "http://xv-s.heteml.jp/skill/logintop_gdod.php";

	private static final String SKILLNOTE_LOGOUT_URL = "http://xv-s.heteml.jp/skill/logout_gdod.php";

	private static final String SKILLNOTE_DIFFICULTYLIST_URL = "http://xv-s.heteml.jp/skill/music_gdod.php?k=";

	private static final String SKILLNOTE_GF_MUSIC_LIST_URL = "http://xv-s.heteml.jp/skill/skilledit_info_gdod.php?gd=g";

	private static final String SKILLNOTE_DM_MUSIC_LIST_URL = "http://xv-s.heteml.jp/skill/skilledit_info_gdod.php?gd=d";

	private static final String SKILLNOTE_GFMODE_URL = "http://xv-s.heteml.jp/skill/skilledit_gdod.php?gd=g";

	private static final String SKILLNOTE_DMMODE_URL = "http://xv-s.heteml.jp/skill/skilledit_gdod.php?gd=d";

	private static final String SKILLNOTE_GFMAIN_URL = "http://xv-s.heteml.jp/skill/skilledit_main_gdod.php?gd=g";

	private static final String SKILLNOTE_DMMAIN_URL = "http://xv-s.heteml.jp/skill/skilledit_main_gdod.php?gd=d";

	private static final String SKILLNOTE_SKILLEDIT_BASE_URL = "http://xv-s.heteml.jp/skill/acvedit_gdod.php";

	private static final String SKILLNOTE_GF_SKILL_LIST_URL = "http://xv-s.heteml.jp/skill/gdod.php?uid=g";

	private static final String SKILLNOTE_DM_SKILL_LIST_URL = "http://xv-s.heteml.jp/skill/gdod.php?uid=d";

}
