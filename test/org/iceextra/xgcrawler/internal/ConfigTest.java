package org.iceextra.xgcrawler.internal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ConfigTest {

	@Test
	public void testDefaultValue() {
		final Config config = new Config("blank");
		assertFalse(config.canAddSkillInfoToComment());
		assertFalse(config.canDeleteAll());
		assertTrue(config.canSynchronizeCompletely());
		assertTrue(config.canUpdateDm());
		assertFalse(config.canUpdateFullcombo());
		assertTrue(config.canUpdateGf());
		assertFalse(config.canUpdateOnlySkillTarget());
	}

	@Test
	public void testAllTrueValue() {
		final Config config = new Config("allTrue");
		assertTrue(config.canAddSkillInfoToComment());
		assertTrue(config.canDeleteAll());
		assertTrue(config.canSynchronizeCompletely());
		assertTrue(config.canUpdateDm());
		assertTrue(config.canUpdateFullcombo());
		assertTrue(config.canUpdateGf());
		assertTrue(config.canUpdateOnlySkillTarget());
	}

	@Test
	public void testAllFalseValue() {
		final Config config = new Config("allFalse");
		assertFalse(config.canAddSkillInfoToComment());
		assertFalse(config.canDeleteAll());
		assertFalse(config.canSynchronizeCompletely());
		assertFalse(config.canUpdateDm());
		assertFalse(config.canUpdateFullcombo());
		assertFalse(config.canUpdateGf());
		assertFalse(config.canUpdateOnlySkillTarget());
	}
}
